#!/bin/bash

# prompt for password
echo "Sudo startup"
[ "$UID" -eq 0 ] || exec sudo "$0" "$@"

chmod 777 "/sys/class/backlight/intel_backlight/brightness"

NOT_EXP_TIME=1000
MESSAGE="Startup script done!"
notify-send "$MESSAGE" -t $NOT_EXP_TIME

exit 1
