#!/bin/bash

#notification expire time
NOT_EXP_TIME=500
TERMINAL="st"
launch()
{
	local MESSAGE="Opening $1..."
	echo "$MESSAGE"
	notify-send "$MESSAGE" -t $NOT_EXP_TIME
	eval "$TERMINAL -e $1"
}

if [ -z "$1" ]
then
	echo "Invalid process to launch. Aborting."
else
	launch "$1"
fi
