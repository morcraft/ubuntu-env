#!/bin/bash

# flip screens
xrandr --output HDMI-1 --primary --right-of DP-1

# set desktop background image
#$HOME/.config/i3/scripts/background.sh 0

# kill instance if any
# pulseaudio --k

# disable all screen/energy saving
xset s off
xset -dpms
xset s noblank

# start pulseaudio daemon
pulseaudio -D &

alsactl init && alsactl store

# for some reason it starts muted sometimes
amixer -D pulse set Master 1+ unmute

# languages daemon
ibus-daemon -d

compton &

killall yabar
yabar &

wal -R -s

$HOME/.config/i3/scripts/set_theme.sh

# dunst &

# run only if on laptop
[ -d /sys/module/battery ] && \
st -e $HOME/.config/i3/scripts/sudo_startup.sh
exit 1
