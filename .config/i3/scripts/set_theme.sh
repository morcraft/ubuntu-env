#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"
. "$__dir/toggle_process.sh"

DUNST_TEMPLATE="$HOME/.config/dunst/dunstrc.template"

DUNST_CONFIG=$(sed -e "s/\${background}/${background}/g" -e "s/\${foreground}/${foreground}/g" -e "s/\${color0}/${color0}/g" -e "s/\${color1}/${color1}/g" -e "s/\${color2}/${color2}/g" $DUNST_TEMPLATE)

BAR_TEMPLATE="$HOME/.config/yabar/yabar.conf.template"
BAR_CONFIG=$(sed -e "s/\${background}/${background:1}/g" -e "s/\${foreground}/${foreground:1}/g" -e "s/\${color0}/${color0:1}/g" -e "s/\${color1}/${color1:1}/g" -e "s/\${color2}/${color2:1}/g" -e "s/\${color3}/${color3:1}/g" -e "s/\${color4}/${color4:1}/g" -e "s/\${color5}/${color5:1}/g" $BAR_TEMPLATE)

I3_TEMPLATE="$HOME/.config/i3/config.template"
I3_CONFIG=$(sed -e "s/\${background}/${background}/g" -e "s/\${foreground}/${foreground}/g" -e "s/\${color0}/${color0}/g" -e "s/\${color1}/${color1}/g" -e "s/\${color2}/${color2}/g" -e "s/\${color3}/${color3}/g" -e "s/\${color4}/${color4}/g" -e "s/\${color5}/${color5}/g" $I3_TEMPLATE)

printf "%s" "$BAR_CONFIG" > "$HOME/.config/yabar/yabar.conf"
printf "%s" "$DUNST_CONFIG" > "$HOME/.config/dunst/dunstrc"
printf "%s" "$I3_CONFIG" > "$HOME/.config/i3/config"

$__dir/set_screensaver.sh

i3-msg reload

toggle_process "yabar" 1
toggle_process "dunst" 1
notify-send "Reloaded theme"
