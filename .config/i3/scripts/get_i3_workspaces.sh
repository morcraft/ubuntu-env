#!/bin/sh

i3lock
return 0
B='#3a3c4eD9'  # blank
C='#ea51b222'  # clear ish
D='#62d6e8D9'  # default
T='#ea51b2D9'  # text #base08
W='#ea51b2D9'  # wrong #base04
V='#b45bcfD9'  # verifying

i3lock \
--insidevercolor=$C   \
--ringvercolor=$V     \
\
--insidewrongcolor=$C \
--ringwrongcolor=$W   \
\
--insidecolor=$B      \
--ringcolor=$D        \
--linecolor=$B        \
--separatorcolor=$D   \
\
--verifcolor=$T        \
--wrongcolor=$T        \
--timecolor=$T        \
--datecolor=$T        \
--layoutcolor=$T      \
--keyhlcolor=$W       \
--bshlcolor=$W        \
\
--screen 1            \
--blur 5              \
--clock               \
--indicator           \
--timestr="%H:%M:%S"  \
--datestr="%A, %m %Y" \
--keylayout 2         \
\
--veriftext="..." \
--wrongtext="Wrong" \
# --textsize=20
# --modsize=10
# --timefont=comic-sans
# --datefont=monofur
# etc
