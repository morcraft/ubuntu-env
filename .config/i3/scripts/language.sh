#!/bin/bash

#notification expire time
NOT_EXP_TIME=800

switch_language()
{
	local MESSAGE="Layout: $1"
	echo "$MESSAGE"
	notify-send "$MESSAGE" -t $NOT_EXP_TIME
	eval "setxkbmap -layout $1"
}

if [ -z "$1" ]
then
	echo "Invalid language to switch to. Aborting."
else 
	switch_language "$1"
fi
