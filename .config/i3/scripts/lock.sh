#!/bin/sh

# shellcheck source=/home/christian/.cache/wal/colors.sh
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

env \
	XSECURELOCK_AUTH_BACKGROUND_COLOR="${color1}" \
	XSECURELOCK_AUTH_FOREGROUND_COLOR="${foreground}" \
	XSECURELOCK_PASSWORD_PROMPT="cursor" \
	XSECURELOCK_SAVER=saver_mpv \
	XSECURELOCK_LIST_VIDEOS_COMMAND="find $HOME/Pictures/Screensavers -type f" \
	XSECURELOCK_SHOW_DATETIME=1 \
	XSECURELOCK_FONT="Hack Regular Nerd Font Mono-10" \
	xsecurelock
