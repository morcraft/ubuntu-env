#!/bin/bash

#notification expire time
NOT_EXP_TIME=1000

launch()
{
	local MESSAGE="Opening $1..."
	echo "$MESSAGE"


	notify-send "$MESSAGE" -t $NOT_EXP_TIME
	eval "$1 &"
}

if [ -z "$1" ]
then
	echo "Invalid process to launch. Aborting."
else (
	launch "$1"
)
fi
