#!/bin/bash

NOT_EXP_TIME=1000
ROOT_PATH="/sys/class/backlight/intel_backlight"
#ROOT_PATH="$HOME/.config/i3/scripts/test/"
MAX_BRIGHTNESS_PATH="${ROOT_PATH}/max_brightness"
BRIGHTNESS_PATH="${ROOT_PATH}/brightness"
BRIGHTNESS_STEP=50
# setting a minimum brightness of 0 will turn the whole screen black
# this is a percentage value!
MINIMUM_BRIGHTNESS_PERCENTAGE=5

get_max_brightness()
{
	read -r line < "$MAX_BRIGHTNESS_PATH"
	printf "%s" "$line"
}

get_brightness()
{
	read -r line < "$BRIGHTNESS_PATH"
	printf "%s" "$line"
}

get_real_brightness()
{
	MAX_BRIGHTNESS="$(get_max_brightness)"
	BRIGHTNESS="$(get_brightness)"
	printf "%s" "$(( BRIGHTNESS * 100 / MAX_BRIGHTNESS ))"
}

set_brightness()
{
	MAX_BRIGHTNESS="$(get_max_brightness)"
	BRIGHTNESS="$(get_brightness)"
	MINIMUM_VALUE="$(( MINIMUM_BRIGHTNESS_PERCENTAGE * MAX_BRIGHTNESS / 100 ))"

	case $1
		in
		up) VALUE="$((BRIGHTNESS + BRIGHTNESS_STEP))";;
		down) VALUE="$((BRIGHTNESS - BRIGHTNESS_STEP))";;
		*) printf "%s" "Invalid argument\\n"
	esac

	if [ "$VALUE" -gt "$MAX_BRIGHTNESS" ]
	then
		VALUE=$MAX_BRIGHTNESS
	elif [ "$VALUE" -lt  "$MINIMUM_VALUE" ]
	then
        VALUE=$MINIMUM_VALUE
	fi

	printf "%s" "$VALUE" > "$BRIGHTNESS_PATH"
	FINAL_BRIGHTNESS="$(get_brightness)"
	PERCENTAGE_VALUE="$(( FINAL_BRIGHTNESS * 100 / MAX_BRIGHTNESS ))"
	MESSAGE="Brightness $PERCENTAGE_VALUE%"
	echo "$MESSAGE"
	notify-send "$MESSAGE" -t $NOT_EXP_TIME
}

case $1
	in
	up) set_brightness "up";;
	down) set_brightness "down";;
esac

