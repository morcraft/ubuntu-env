#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

. "$__dir/../brightness.sh"

[ -z "$1" ] && ICON="" || ICON="$1"
OUTPUT=$(get_real_brightness)
printf "%s" "$ICON $OUTPUT"
