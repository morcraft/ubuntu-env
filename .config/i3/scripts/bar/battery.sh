#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/../default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

get_charge(){
    acpi -b | awk "{print $1}" | sed 's/\([^:]*\): \([^,]*\), \([0-9]*\)%.*/\3/'
}

get_status(){
    acpi -b | awk "{print $1}" | sed 's/\([^:]*\): \([^,]*\), \([0-9]*\)%.*/\2/'
}

is_charging(){
   [ $(get_status) == "Charging" ] && printf "1" || printf "0"
}

get_icon(){
    [ $(is_charging) -eq 1 ] && { printf ""; return 0; }
    CHARGE=$(get_charge)
    [ "$CHARGE" -lt 25 ] && { printf ""; return 0; }
    [ "$CHARGE" -lt 50 ] && { printf ""; return 0; }
    [ "$CHARGE" -lt 75 ] && { printf ""; return 0; }
    [ "$CHARGE" -lt 75 ] && { printf ""; return 0; }
    printf ""
}

get_color()
{
    [ $(is_charging) -eq 1 ] && { printf "!Y BG 0x50${color3:1} Y!"; return 0; }
    [ $(get_charge) -lt 25 ] && printf "!Y BG 0x50${color2:1} Y!"
}

CHARGE="$(get_charge)"
ICON=$(get_icon)
OUTPUT="$CHARGE%"
COLOR=$(get_color)
printf "%s" "$COLOR $ICON $OUTPUT"
