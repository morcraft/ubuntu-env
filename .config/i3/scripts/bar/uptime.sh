#!/bin/bash

[ -z "$1" ] && ICON="" || ICON="$1"
# https://unix.stackexchange.com/a/175109
secondsToTime ()
{
   T=$1
   D=$((T/60/60/24))
   H=$((T/60/60%24))
   M=$((T/60%60))
   S=$((T%60))

   if [[ ${D} != 0 ]]
   then
      printf '%d days %02d:%02d:%02d' $D $H $M $S
   else
      printf '%02d:%02d:%02d' $H $M $S
   fi
}

UPTIME=$(cat /proc/uptime | awk '{printf $1}' | awk -F . '{printf $1}')
OUTPUT=$(secondsToTime "$UPTIME")
printf "%s" "$ICON $OUTPUT"
