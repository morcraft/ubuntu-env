#!/bin/bash

#notification expire time
NOT_EXP_TIME=500

MESSAGE="Opening Ranger..."
echo "$MESSAGE"
notify-send "$MESSAGE" -t "$NOT_EXP_TIME"
# urxvt -e ranger
st -e bash -i -c "ranger --choosedir=$HOME/.rangerdir; LASTDIR=$(cat "$HOME"/.rangerdir);cd $LASTDIR"
