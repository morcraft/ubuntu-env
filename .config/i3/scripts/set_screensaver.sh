#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

CURRENT_BG=$(cat "$HOME/.cache/wal/wal")
FILENAME=$(basename -- "$CURRENT_BG")
mkdir -p $HOME/Pictures/Screensavers
rm -rf $HOME/Pictures/Screensavers/*
echo "Current bg: $CURRENT_BG"
notify-send "$CURRENT_BG"
#convert "$CURRENT_BG" -resize 1920x1080 "$HOME/Pictures/Screensavers/$FILENAME"
convert "$CURRENT_BG" -colorspace gray -fill "${color1}" -tint 90 -bordercolor "${color1}" -border 20x20 "$HOME/Pictures/Screensavers/$FILENAME"

VIDEO=$(find $HOME/Videos -type f | head -1)
[ -z "$VIDEO" ] && printf "%s" "No found videos under $HOME/Videos\\n"; return 1;

SIZE=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$VIDEO")

mkdir -p $HOME/Videos/Screensavers
rm -rf $HOME/Videos/Screensavers/*

#ffmpeg -i "$VIDEO" -f lavfi -i "color=${color1}:s=$SIZE" -filter_complex "[0:0]colorchannelmixer=.3:.4:.3:0:.3:.4:.3:0:.3:.4:.3;[1:0]blend=shortest=1:all_mode=overlay:all_opacity=0.7" $HOME/Videos/Screensavers/default.mp4
