#!/bin/bash

#notification expire time
NOT_EXP_TIME=500

MESSAGE="Opening i3 configuration..."
echo "$MESSAGE"
notify-send "$MESSAGE" -t $NOT_EXP_TIME
# urxvt -e "vim ~/.config/i3/config"
st -e nvim $HOME/.config/i3/config.template
