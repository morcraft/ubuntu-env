#!/bin/bash

#notification expire time
NOT_EXP_TIME=1500
EXTENSIONS="\.jpeg|\.jpg|\.png"
WALLPAPERS_PATH="$HOME/Pictures/Wallpapers"

get_wallpapers(){
	echo `ls $WALLPAPERS_PATH/* | egrep $EXTENSIONS`
}

switch_background()
{
	local WALLPAPERS=($(get_wallpapers))
	local MESSAGE="Set background to: ${WALLPAPERS[$1]}"
	eval "feh --bg-center --bg-fill ${WALLPAPERS[$1]}"
	echo "$MESSAGE"
	notify-send "$MESSAGE" -t $NOT_EXP_TIME
}

declare MESSAGE

if [ -z "$1" ]
then
	MESSAGE="Invalid background image target"
else 
	switch_background "$1"
fi

if [ ! -z "$MESSAGE" ]
then (
	echo $MESSAGE
	notify-send "$MESSAGE" -t $NOT_EXP_TIME
)
fi
