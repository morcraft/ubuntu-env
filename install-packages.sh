#!/bin/bash

echo "Starting environment configuration..."
[ "$UID" -eq 0 ] || exec sudo "$0" "$@"

sudo apt-get update
sudo apt-get install -y git \
cmake \
curl \
i3 \
i3blocks \
fzf \
rxvt-unicode \
vim \
mpv \
mplayer \
ranger \
feh \
xclip \
mupdf \
gnome-calculator \
pulsemixer \
python3-pip \
qutebrowser \
hostapd \
ibus-unikey \
python \
build-essential \
python3-dev \
silversearcher-ag \
imagemagick \
compton \
libtool \
libtool-bin

# yabar
sudo apt install -y libcairo2-dev \
libpango1.0-dev \
libconfig-dev \
libxcb-randr0-dev \
libxcb-ewmh-dev \
libxcb-icccm4-dev \
libgdk-pixbuf2.0-dev \
libasound2-dev \
libiw-dev \
libxkbcommon-dev \
libxkbcommon-x11-dev \
libxcb-xkb-dev

# xsecurelock
sudo apt install -y apache2-utils \
autotools-dev \
binutils \
gcc \
libc6-dev \
libpam-dev \
libx11-dev \
libxcomposite-dev \
libxext-dev \
libxfixes-dev \
libxmuu-dev \
libxrandr-dev \
libxss-dev \
make \
mplayer \
mpv \
pamtester \
pkg-config \
x11proto-core-dev \
xscreensaver

pip3 install mps-youtube \
youtube-dl

# lazygit
sudo add-apt-repository -y ppa:lazygit-team/release
sudo apt-get install -y lazygit

# nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

# vim plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim +PlugInstall +qall

command -v nvm
nvm install node
nvm alias default 6.1.0
npm install typescript -g

python3 $HOME/.vim/plugged/youcompleteme/install.py --ts-completer --clang-completer

sudo snap install code --classic \
postman \
gimp \
inkscape

echo "Creating additional packages and building them..."
mkdir -p "$HOME/dev/"

sudo add-apt-repository -y ppa:dhor/myway
sudo apt-get install -y xnconvert

source $HOME/.bashrc
xrdb $HOME/.Xresources

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb

echo "Remember to install dunst, i3lock-color and sc-im"
